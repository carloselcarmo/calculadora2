package br.com.tdd;

import java.math.BigDecimal;

public class Calculadora
{
    public static Integer soma(Integer primeiroNumero, Integer segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return soma(num1, num2).intValue();
    }

    public static Double soma(Double primeiroNumero, Double segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return soma(num1, num2).doubleValue();
    }

    private static BigDecimal soma(BigDecimal primeiroNumero, BigDecimal segundoNumero)
    {
        return primeiroNumero.add(segundoNumero);
    }

    public static Integer subtracao(Integer primeiroNumero, Integer segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return subtracao(num1,num2).intValue();
    }

    public static Double subtracao(Double primeiroNumero, Double segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return subtracao(num1,num2).doubleValue();
    }

    private static BigDecimal subtracao(BigDecimal primeiroNumero, BigDecimal segundoNumero)
    {
        return primeiroNumero.subtract(segundoNumero);
    }

    public static Integer multiplicar(Integer primeiroNumero, Integer segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return multiplicar(num1, num2).intValue();
    }

    public static Double multiplicar(Double primeiroNumero, Double segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return multiplicar(num1, num2).doubleValue();
    }

    private static BigDecimal multiplicar(BigDecimal primeiroNumero, BigDecimal segundoNumero)
    {
        return primeiroNumero.multiply(segundoNumero);
    }

    public static Integer dividir(Integer primeiroNumero, Integer segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return dividir(num1, num2).intValue();
    }

    public static Double dividir(Double primeiroNumero, Double segundoNumero)
    {
        BigDecimal num1 = new BigDecimal(primeiroNumero);
        BigDecimal num2 = new BigDecimal(segundoNumero);
        return dividir(num1, num2).doubleValue();
    }

    private static BigDecimal dividir(BigDecimal primeiroNumero, BigDecimal segundoNumero)
    {
        if(segundoNumero !=  BigDecimal.ZERO)
        {
            return primeiroNumero.divide(segundoNumero);
        }
        else
        {
            throw new RuntimeException("Não pode ocorrer divisão por zero");
        }
    }
}
