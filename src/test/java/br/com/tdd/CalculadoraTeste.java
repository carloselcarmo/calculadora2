package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste
{
    @Test
    public void testarSomaDeDoisNumerosInteiros()
    {
        //Preparação
        Integer primeiroNumero = 2;
        Integer segundoNumero = 2;
        Integer resultadoEsperado = 4;

        //Execução
        Integer Resultado = Calculadora.soma(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarSomaDeDoisNumerosFlutuantes()
    {
        //Preparação
        Double primeiroNumero = 2.1;
        Double segundoNumero = 2.6;
        Double resultadoEsperado = 4.7;

        //Execução
        Double Resultado = Calculadora.soma(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarSubtracaoDeDoisNumerosInteiros()
    {
        //Preparação
        Integer primeiroNumero = 2;
        Integer segundoNumero = 2;
        Integer resultadoEsperado = 0;

        //Execução
        Integer Resultado = Calculadora.subtracao(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarSubtracaoDeDoisNumerosFlutuantes()
    {
        //Preparação
        Double primeiroNumero = 2.4;
        Double segundoNumero = 1.4;
        Double resultadoEsperado = 1.0;

        //Execução
        Double Resultado = Calculadora.subtracao(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosInteiros()
    {
        //Preparação
        Integer primeiroNumero = 5;
        Integer segundoNumero = 2;
        Integer resultadoEsperado = 10;

        //Execução
        Integer Resultado = Calculadora.multiplicar(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosFlutuantes()
    {
        //Preparação
        Double primeiroNumero = 2.5;
        Double segundoNumero = 2.0;
        Double resultadoEsperado = 5.0;

        //Execução
        Double Resultado = Calculadora.multiplicar(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosInteiros()
    {
        //Preparação
        Integer primeiroNumero = 10;
        Integer segundoNumero = 5;
        Integer resultadoEsperado = 2;

        //Execução
        Integer Resultado = Calculadora.dividir(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarDivisaoPorZeroComNumerosInteiros ()
    {
        //Preparação
        Integer primeiroNumero = 10;
        Integer segundoNumero = 0;

        //Execução e validação
        Assertions.assertThrows(RuntimeException.class, () -> { Calculadora.dividir(primeiroNumero, segundoNumero);});
    }

    @Test
    public void testarDivisaoDeDoisNumerosFlutuantes()
    {
        //Preparação
        Double primeiroNumero = 5.0;
        Double segundoNumero = 2.0;
        Double resultadoEsperado = 2.5;

        //Execução
        Double Resultado = Calculadora.dividir(primeiroNumero, segundoNumero);

        //Verificação
        Assertions.assertEquals(resultadoEsperado, Resultado);
    }

    @Test
    public void testarDivisaoPorZeroComNumerosFlutuantes()
    {
        //Preparação
        Double primeiroNumero = 5.0;
        Double segundoNumero = 0.0;

        //Execução e validação
        Assertions.assertThrows(RuntimeException.class, () -> { Calculadora.dividir(primeiroNumero, segundoNumero);});
    }
}
